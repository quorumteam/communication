// Bundle of functions managing the CRUD and the elasticsearch engine
package controllers

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/quorumteam/communication/models"
	"github.com/quorumsco/logs"
)

// Email contains the email related methods and a gorm client
type Email struct {
	DB *gorm.DB
}

// RetrieveCollection calls the EmailSQL Find method and returns the results via RPC
func (t *Email) RetrieveCollection(args models.EmailArgs, reply *models.EmailReply) error {
	var (
		emailStore = models.EmailStore(t.DB)
		err       error
	)

	if reply.Emails, err = emailStore.Find(args); err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// // RetrieveCollection by Type calls the EmailSQL Find method and returns the results via RPC
// func (t *Email) RetrieveCollectionByType(args models.EmailArgs, reply *models.EmailReply) error {
// 	var (
// 		emailStore = models.EmailStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Emails, err = emailStore.FindByType(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }
//
// // RetrieveCollection by Name calls the EmailSQL Find method and returns the results via RPC
// func (t *Email) RetrieveCollectionByName(args models.EmailArgs, reply *models.EmailReply) error {
// 	var (
// 		emailStore = models.EmailStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Emails, err = emailStore.FindByName(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }
//
// // RetrieveCollection by Type and Name calls the EmailSQL Find method and returns the results via RPC
// func (t *Email) RetrieveCollectionByTypeAndName(args models.EmailArgs, reply *models.EmailReply) error {
// 	var (
// 		emailStore = models.EmailStore(t.DB)
// 		err       error
// 	)
//
// 	if reply.Emails, err = emailStore.FindByTypeAndName(args); err != nil {
// 		logs.Error(err)
// 		return err
// 	}
//
// 	return nil
// }

// Retrieve calls the EmailSQL First method and returns the results via RPC
func (t *Email) Retrieve(args models.EmailArgs, reply *models.EmailReply) error {
	var (
		emailStore = models.EmailStore(t.DB)
		err       error
	)

	if reply.Email, err = emailStore.First(args); err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// Update calls the EmailSQL Update method and returns the results via RPC
func (t *Email) Update(args models.EmailArgs, reply *models.EmailReply) error {
	var (
		emailStore = models.EmailStore(t.DB)
		err       error
	)

	if err = emailStore.Save(args.Email, args); err != nil {
		logs.Error(err)
		return err
	}

	if reply.Email, err = emailStore.First(args); err != nil {
		return err
	}

	return nil
}

// Create calls the EmailSQL Create method and returns the results via RPC
func (t *Email) Create(args models.EmailArgs, reply *models.EmailReply) error {
	var (
		emailStore = models.EmailStore(t.DB)
		err       error
	)

	if err = emailStore.Save(args.Email, args); err != nil {
		logs.Error(err)
		return err
	}

	reply.Email = args.Email

	return nil
}

// Delete calls the EmailSQL Delete method and returns the results via RPC
func (t *Email) Delete(args models.EmailArgs, reply *models.EmailReply) error {
	var (
		emailStore = models.EmailStore(t.DB)
		err       error
	)

	if err = emailStore.Delete(args.Email, args); err != nil {
		logs.Debug(err)
		return err
	}

	return nil
}
