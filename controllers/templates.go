// Bundle of functions managing the CRUD and the elasticsearch engine
package controllers

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/quorumteam/communication/models"
	"github.com/quorumsco/logs"
)

// Template contains the Template related methods and a gorm client
type Template struct {
	DB *gorm.DB
}

// RetrieveCollection calls the TemplateSQL Find method and returns the results via RPC
func (t *Template) RetrieveCollection(args models.TemplateArgs, reply *models.TemplateReply) error {
	var (
		err error

		TemplateStore = models.TemplateStore(t.DB)
	)

	reply.Templates, err = TemplateStore.Find(args)
	if err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// Retrieve calls the TemplateSQL First method and returns the results via RPC
func (t *Template) Retrieve(args models.TemplateArgs, reply *models.TemplateReply) error {
	var (
		TemplateStore = models.TemplateStore(t.DB)
		err           error
	)

	if reply.Template, err = TemplateStore.First(args); err != nil {
		logs.Error(err)
		return err
	}

	return nil
}

// Create calls the TemplateSQL Save method and returns the results via RPC
func (t *Template) Create(args models.TemplateArgs, reply *models.TemplateReply) error {
	var (
		err error

		TemplateStore = models.TemplateStore(t.DB)
	)

	if err = TemplateStore.Save(args.Template, args); err != nil {
		logs.Error(err)
		return err
	}

	reply.Template = args.Template

	return nil
}

// Delete calls the TemplateSQL Delete method and returns the results via RPC
func (t *Template) Delete(args models.TemplateArgs, reply *models.TemplateReply) error {
	var (
		err error

		TemplateStore = models.TemplateStore(t.DB)
	)

	if err = TemplateStore.Delete(args.Template, args); err != nil {
		logs.Debug(err)
		return err
	}

	return nil
}
