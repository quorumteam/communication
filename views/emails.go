// Views for JSON responses
package views

import "bitbucket.org/quorumteam/communication/models"

// Forms is a type used for JSON request responses
type Emails struct {
	Emails []models.Email `json:"emails"`
}

// Form is a type used for JSON request responses
type Email struct {
	Email *models.Email `json:"email"`
}
