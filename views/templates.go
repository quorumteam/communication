// Views for JSON responses
package views

import "bitbucket.org/quorumteam/communication/models"

// Templates is a type used for JSON request responses
type Templates struct {
	Templates []models.Template `json:"templates"`
}

// Template is a type used for JSON request responses
type Template struct {
	Template *models.Template `json:"template"`
}
