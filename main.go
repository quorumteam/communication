// Contact managing structure, it does all the database CRUD interactions and contains an implementation of elasticsearch engine
package main

import (
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"runtime"
	"time"

	"github.com/codegangsta/cli"
	"github.com/jinzhu/gorm"
	"github.com/quorumsco/cmd"
	"github.com/quorumsco/databases"
	"bitbucket.org/quorumteam/communication/controllers"
	"bitbucket.org/quorumteam/communication/models"
	"github.com/quorumsco/logs"
	"github.com/quorumsco/settings"
)

var (
	//TIMEOUT time between each try
	TIMEOUT = 5 * time.Second
	//RETRY number of tries
	RETRY = 3
)

func init() {
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	cmd := cmd.New()
	cmd.Name = "communication"
	cmd.Usage = "quorums communication backend"
	cmd.Version = "0.0.1"
	cmd.Before = serve
	cmd.Flags = append(cmd.Flags, []cli.Flag{
		cli.StringFlag{Name: "config, c", Usage: "configuration file", EnvVar: "CONFIG"},
		cli.HelpFlag,
	}...)
	cmd.RunAndExitOnError()
}

// Definition of the GORM and Elasticsearch clients and Registration of the functions to RPC with the said clients
func serve(ctx *cli.Context) error {
	var (
		config settings.Config
		err    error
	)

	if ctx.String("config") != "" {
		config, err = settings.Parse(ctx.String("config"))
		if err != nil {
			logs.Error(err)
		}
	}

	if config.Debug() {
		logs.Level(logs.DebugLevel)
	}

	dialect, args, err := config.SqlDB()
	if err != nil {
		logs.Critical(err)
		os.Exit(1)
	}
	logs.Debug("database type: %s", dialect)

	var db *gorm.DB
	if db, err = databases.InitGORM(dialect, args); err != nil {
		logs.Critical(err)
		os.Exit(1)
	}

	logs.Debug("connected to %s", args)

	if config.Migrate() {
		db.AutoMigrate(models.Models()...)
		logs.Debug("database migrated successfully")
	}

	if config.Debug() {
		db.LogMode(true)
	}

	var server settings.Server
	server, err = config.Server()
	if err != nil {
		logs.Critical(err)
		os.Exit(1)
	}

	rpc.Register(&controllers.Email{DB: db})
	rpc.Register(&controllers.Template{DB: db})

	rpc.HandleHTTP()

	l, e := net.Listen("tcp", server.String())
	if e != nil {
		log.Fatal("listen error:", e)
	}
	logs.Info("Listening on " + server.String())
	return http.Serve(l, nil)
}
