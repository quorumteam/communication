// Definition of the structures and SQL interaction functions
package models

import "github.com/jinzhu/gorm"

// RefvalueDS implements the RefvalueSQL methods
type RefvalueDS interface {
	Save(*Refvalue, RefvalueArgs) error
	Delete(*Refvalue, RefvalueArgs) error
	First(RefvalueArgs) (*Refvalue, error)
	Find(RefvalueArgs) ([]Refvalue, error)
}

// Refvaluestore returns a RefvalueDS implementing CRUD methods for the refvalues and containing a gorm client
func RefvalueStore(db *gorm.DB) RefvalueDS {
	return &RefvalueSQL{DB: db}
}
