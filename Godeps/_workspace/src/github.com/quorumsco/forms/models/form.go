// Definition of the structures and SQL interaction functions
package models

// Form represents all the components of a form
type Form struct {
	ID       uint   `gorm:"primary_key" db:"id" json:"id"`
	Name     string `db:"name" json:"name"`
	Type     string `db:"type" json:"type"`
	Label    string `db:"label" json:"label"`
	Section	string `db:"section" json:"section"`
	Activate string `db:"activate" json:"activate"`
	Order uint `db:"order" json:"order"`

	GroupID uint `sql:"not null" db:"group_id" json:"group_id"`

	Refvalues []Refvalue `json:"refvalues,omitempty" gorm:"ForeignKey:FormID"`
}

// FormArgs is used in the RPC communications between the gateway and Forms
type FormArgs struct {
	Form *Form
}

// FormReply is used in the RPC communications between the gateway and Forms
type FormReply struct {
	Form  *Form
	Forms []Form
}

// Validate checks if the contact is valid
func (c *Form) Validate() map[string]string {
	var errs = make(map[string]string)

	// if c.Firstname == "" {
	// 	errs["firstname"] = "is required"
	// }

	// if c.Surname == "" {
	// 	errs["surname"] = "is required"
	// }

	// if c.Mail != nil && !govalidator.IsEmail(*c.Mail) {
	// 	errs["mail"] = "is not valid"
	// }

	return errs
}
