// Definition of the structures and SQL interaction functions
package models

// Refvalue represents the components of a refvalue
type Refvalue struct {
	ID     uint   `db:"id" json:"id"`
	FormId uint   `db:"form_id" json:"form_id"`
	Label  string `db:"label" json:"label"`
	Value  string `db:"value" json:"value"`
	Min	string	`db:"min" json:"min,omitempty"`
	Max	string	`db:"max" json:"max,omitempty"`
	Step	string	`db:"step" json:"step,omitempty"`

	GroupID uint `db:"group_id" json:"group_id"`
}

// RefvalueArgs is used in the RPC communications between the gateway and Contacts
type RefvalueArgs struct {
	GroupID  uint
	FormID   uint
	Refvalue *Refvalue
}

// RefvalueReply is used in the RPC communications between the gateway and Contacts
type RefvalueReply struct {
	Refvalue  *Refvalue
	Refvalues []Refvalue
}
