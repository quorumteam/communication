// Definition of the structures and SQL interaction functions
package models

import "github.com/jinzhu/gorm"

// FormDS implements the FormSQL methods
type FormDS interface {
	Save(*Form, FormArgs) error
	Delete(*Form, FormArgs) error
	First(FormArgs) (*Form, error)
	Find(FormArgs) ([]Form, error)
	FindByType(FormArgs) ([]Form, error)
	FindByName(FormArgs) ([]Form, error)
	FindByTypeAndName(FormArgs) ([]Form, error)

	// FindNotes(*Form, *FormArgs) error
	// FindTags(*Form) error
}

// Formstore returns a FormDS implementing CRUD methods for the forms and containing a gorm client
func FormStore(db *gorm.DB) FormDS {
	return &FormSQL{DB: db}
}
