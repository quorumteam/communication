// Definition of the structures and SQL interaction functions
package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// RefvalueSQL contains a Gorm client and the refvalue and gorm related methods
type RefvalueSQL struct {
	DB *gorm.DB
}

// Save inserts a new refvalue into the database
func (s *RefvalueSQL) Save(n *Refvalue, args RefvalueArgs) error {
	if n == nil {
		return errors.New("save: refvalue is nil")
	}

	n.GroupID = args.Refvalue.GroupID
	if n.ID == 0 {
		err := s.DB.Create(n).Error
		s.DB.Last(n)
		return err
	}

	return s.DB.Where("group_id = ?", args.Refvalue.GroupID).Save(n).Error
}

// Delete removes a refvalue from the database
func (s *RefvalueSQL) Delete(n *Refvalue, args RefvalueArgs) error {
	if n == nil {
		return errors.New("delete: refvalue is nil")
	}

	return s.DB.Where("group_id = ?", args.Refvalue.GroupID).Delete(n).Error
}

// First returns a refvalue from the database usin it's ID
func (s *RefvalueSQL) First(args RefvalueArgs) (*Refvalue, error) {
	var n Refvalue

	if err := s.DB.Where(args.Refvalue).First(&n).Error; err != nil {
		if s.DB.Where(args.Refvalue).First(&n).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	return &n, nil
}

// Find returns all the refvalues containing a given groupID from the database
func (s *RefvalueSQL) Find(args RefvalueArgs) ([]Refvalue, error) {
	var refvalues []Refvalue

	err := s.DB.Where("group_id = ?", args.Refvalue.GroupID).Where("form_id = ?", args.FormID).Find(&refvalues).Error
	if err != nil {
		return nil, err
	}

	return refvalues, nil
}
