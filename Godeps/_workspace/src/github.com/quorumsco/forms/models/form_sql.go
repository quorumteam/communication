// Definition of the structures and SQL interaction functions
package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// FormSQL contains a Gorm client and the form and gorm related methods
type FormSQL struct {
	DB *gorm.DB
}

// Save inserts a new form into the database
func (s *FormSQL) Save(c *Form, args FormArgs) error {
	if c == nil {
		return errors.New("save: form is nil")
	}

	c.GroupID = args.Form.GroupID
	if c.ID == 0 {
		err := s.DB.Create(c).Error
		s.DB.Last(c)
		return err
	}

	return s.DB.Where("group_id = ?", args.Form.GroupID).Save(c).Error
}

// Delete removes a form from the database
func (s *FormSQL) Delete(c *Form, args FormArgs) error {
	if c == nil {
		return errors.New("delete: form is nil")
	}

	return s.DB.Where("group_id = ?", args.Form.GroupID).Delete(c).Error
}

// First returns a form from the database using his ID
func (s *FormSQL) First(args FormArgs) (*Form, error) {
	var f Form
	//var r Refvalue

	if err := s.DB.Where(args.Form).First(&f).Error; err != nil {
		if s.DB.Where(args.Form).First(&f).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	if err := s.DB.Where("form_id = ?",f.ID).Find(&f.Refvalues).Error; err != nil {
		//if s.DB.Where(c.AddressID).First(&c.Address).RecordNotFound() {
		if s.DB.Where("form_id = ?",f.ID).Find(&f.Refvalues).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	return &f, nil
}


// Find returns all the forms with a given groupID from the database
func (s *FormSQL) Find(args FormArgs) ([]Form, error) {
	var forms []Form

	err := s.DB.Where("group_id = ?", args.Form.GroupID).Find(&forms).Error
	if err != nil {
		return nil, err
	}

	return forms, nil
}

// Find returns all the forms with a given groupID from the database
func (s *FormSQL) FindByType(args FormArgs) ([]Form, error) {
	var forms []Form

	err := s.DB.Where("group_id = ?", args.Form.GroupID).Where("type = ?", args.Form.Type).Find(&forms).Error
	if err != nil {
		return nil, err
	}

	return forms, nil
}

// Find returns all the forms with a given groupID and a given name from the database
func (s *FormSQL) FindByName(args FormArgs) ([]Form, error) {
	var forms []Form
	//var refvalues []Refvalue

	//db.Preload("Town").Find(&places)
	//err := s.DB.Where("group_id = ?", args.Form.GroupID).Where("name = ?", args.Form.Name).Find(&forms).Error
	err := s.DB.Where("group_id = ?", args.Form.GroupID).Where("name = ?", args.Form.Name).Preload("Refvalues").Find(&forms).Error
	if err != nil {
		return nil, err
	}
	// for i, _ := range forms {
  //   s.DB.Model(forms[i]).Related(&forms[i].Refvalues)
	// }
	//s.DB.Model(&forms).Related(&refvalues)
	return forms, nil
}

// Find returns all the forms with a given groupID from the database
func (s *FormSQL) FindByTypeAndName(args FormArgs) ([]Form, error) {
	var forms []Form

	err := s.DB.Where("group_id = ?", args.Form.GroupID).Where("type = ?", args.Form.Type).Where("name = ?", args.Form.Name).Find(&forms).Error
	if err != nil {
		return nil, err
	}

	return forms, nil
}
