// Definition of the structures and SQL interaction functions
package models

import (
	"errors"

	"github.com/jinzhu/gorm"
)

// EmailSQL contains a Gorm client and the email and gorm related methods
type EmailSQL struct {
	DB *gorm.DB
}

// Save inserts a new email into the database
func (s *EmailSQL) Save(c *Email, args EmailArgs) error {
	if c == nil {
		return errors.New("save: email is nil")
	}

	c.GroupID = args.Email.GroupID
	if c.ID == 0 {
		err := s.DB.Create(c).Error
		s.DB.Last(c)
		return err
	}

	return s.DB.Where("group_id = ?", args.Email.GroupID).Save(c).Error
}

// Delete removes a email from the database
func (s *EmailSQL) Delete(c *Email, args EmailArgs) error {
	if c == nil {
		return errors.New("delete: email is nil")
	}

	return s.DB.Where("group_id = ?", args.Email.GroupID).Delete(c).Error
}

// First returns a email from the database using his ID
func (s *EmailSQL) First(args EmailArgs) (*Email, error) {
	var f Email
	//var r Refvalue

	if err := s.DB.Where(args.Email).First(&f).Error; err != nil {
		if s.DB.Where(args.Email).First(&f).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	return &f, nil
}


// Find returns all the emails with a given groupID from the database
func (s *EmailSQL) Find(args EmailArgs) ([]Email, error) {
	var emails []Email

	err := s.DB.Where("group_id = ?", args.Email.GroupID).Find(&emails).Error
	if err != nil {
		return nil, err
	}

	return emails, nil
}
//
// // Find returns all the emails with a given groupID from the database
// func (s *EmailSQL) FindByType(args EmailArgs) ([]Email, error) {
// 	var emails []Email
//
// 	err := s.DB.Where("group_id = ?", args.Email.GroupID).Where("type = ?", args.Email.Type).Find(&emails).Error
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return emails, nil
// }
//
// // Find returns all the emails with a given groupID and a given name from the database
// func (s *EmailSQL) FindByName(args EmailArgs) ([]Email, error) {
// 	var emails []Email
// 	//var refvalues []Refvalue
//
// 	//db.Preload("Town").Find(&places)
// 	//err := s.DB.Where("group_id = ?", args.Email.GroupID).Where("name = ?", args.Email.Name).Find(&emails).Error
// 	err := s.DB.Where("group_id = ?", args.Email.GroupID).Where("name = ?", args.Email.Name).Preload("Refvalues").Find(&emails).Error
// 	if err != nil {
// 		return nil, err
// 	}
// 	// for i, _ := range emails {
//   //   s.DB.Model(emails[i]).Related(&emails[i].Refvalues)
// 	// }
// 	//s.DB.Model(&emails).Related(&refvalues)
// 	return emails, nil
// }
//
// // Find returns all the emails with a given groupID from the database
// func (s *EmailSQL) FindByTypeAndName(args EmailArgs) ([]Email, error) {
// 	var emails []Email
//
// 	err := s.DB.Where("group_id = ?", args.Email.GroupID).Where("type = ?", args.Email.Type).Where("name = ?", args.Email.Name).Find(&emails).Error
// 	if err != nil {
// 		return nil, err
// 	}
//
// 	return emails, nil
// }
