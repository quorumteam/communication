// Definition of the structures and SQL interaction functions
package models

import "github.com/jinzhu/gorm"

// TemplateDS implements the TemplateSQL methods
type TemplateDS interface {
	Save(*Template, TemplateArgs) error
	Delete(*Template, TemplateArgs) error
	First(TemplateArgs) (*Template, error)
	Find(TemplateArgs) ([]Template, error)
}

// Templatestore returns a TemplateDS implementing CRUD methods for the Templates and containing a gorm client
func TemplateStore(db *gorm.DB) TemplateDS {
	return &TemplateSQL{DB: db}
}
