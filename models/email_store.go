// Definition of the structures and SQL interaction functions
package models

import "github.com/jinzhu/gorm"

// EmailDS implements the EmailSQL methods
type EmailDS interface {
	Save(*Email, EmailArgs) error
	Delete(*Email, EmailArgs) error
	First(EmailArgs) (*Email, error)
	Find(EmailArgs) ([]Email, error)
	// FindByType(EmailArgs) ([]Email, error)
	// FindByName(EmailArgs) ([]Email, error)
	// FindByTypeAndName(EmailArgs) ([]Email, error)

	// FindNotes(*Email, *EmailArgs) error
	// FindTags(*Email) error
}

// Emailstore returns a EmailDS implementing CRUD methods for the forms and containing a gorm client
func EmailStore(db *gorm.DB) EmailDS {
	return &EmailSQL{DB: db}
}
