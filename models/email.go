// Definition of the structures and SQL interaction functions
package models

// Email represents all the components of a form
type Email struct {
	ID       uint   `gorm:"primary_key" db:"id" json:"id"`
	Sender     string `db:"sender" json:"sender"`
	Subject     string `db:"subject" json:"subject"`
	Object    string `db:"object" json:"object"`
	Receiver    string `db:"receiver" json:"receiver"`
	Surname    string `db:"surname" json:"surname"`
	Firstname    string `db:"firstname" json:"firstname"`
	Gender    string `db:"gender" json:"gender"`

	GroupID uint `sql:"not null" db:"group_id" json:"group_id"`
}

// EmailArgs is used in the RPC communications between the gateway and Emails
type EmailArgs struct {
	Email *Email
}

// EmailReply is used in the RPC communications between the gateway and Emails
type EmailReply struct {
	Email  *Email
	Emails []Email
}

// Validate checks if the contact is valid
func (c *Email) Validate() map[string]string {
	var errs = make(map[string]string)

	// if c.Firstname == "" {
	// 	errs["firstname"] = "is required"
	// }

	// if c.Surname == "" {
	// 	errs["surname"] = "is required"
	// }

	// if c.Mail != nil && !govalidator.IsEmail(*c.Mail) {
	// 	errs["mail"] = "is not valid"
	// }

	return errs
}
