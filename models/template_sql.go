// Definition of the structures and SQL interaction functions
package models

import (
	"errors"
	"github.com/jinzhu/gorm"
)

// TemplateSQL contains a Gorm client and the Template and gorm related methods
type TemplateSQL struct {
	DB *gorm.DB
}

// Save inserts a new Template into the database
func (s *TemplateSQL) Save(n *Template, args TemplateArgs) error {
	if n == nil {
		return errors.New("save: Template is nil")
	}

	n.GroupID = args.Template.GroupID
	if n.ID == 0 {
		err := s.DB.Create(n).Error
		s.DB.Last(n)
		return err
	}

	return s.DB.Where("group_id = ?", args.Template.GroupID).Save(n).Error
}

// Delete removes a Template from the database
func (s *TemplateSQL) Delete(n *Template, args TemplateArgs) error {
	if n == nil {
		return errors.New("delete: Template is nil")
	}

	return s.DB.Where("group_id = ?", args.Template.GroupID).Delete(n).Error
}

// First returns a Template from the database usin it's ID
func (s *TemplateSQL) First(args TemplateArgs) (*Template, error) {
	var n Template

	if err := s.DB.Where(args.Template).First(&n).Error; err != nil {
		if s.DB.Where(args.Template).First(&n).RecordNotFound() {
			return nil, nil
		}
		return nil, err
	}

	return &n, nil
}

// Find returns all the Templates containing a given groupID from the database
func (s *TemplateSQL) Find(args TemplateArgs) ([]Template, error) {
	var Templates []Template

	err := s.DB.Where("group_id = ?", args.Template.GroupID).Find(&Templates).Error
	if err != nil {
		return nil, err
	}

	return Templates, nil
}
