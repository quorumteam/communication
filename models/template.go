// Definition of the structures and SQL interaction functions
package models

// Template represents the components of a Template
type Template struct {
	ID     uint   `db:"id" json:"id"`
	Sender  string `db:"sender" json:"sender"`
	Object  string `db:"object" json:"object"`
	Subject  string `db:"subject" json:"subject"`
	Gender_label_homme  string `db:"gender_label_homme" json:"gender_label_homme"`
	Gender_label_femme  string `db:"gender_label_femme" json:"gender_label_femme"`
	Gender_label_neutre  string `db:"gender_label_neutre" json:"gender_label_neutre"`
	GroupID uint `db:"group_id" json:"group_id"`
}

// TemplateArgs is used in the RPC communications between the gateway and Communication
type TemplateArgs struct {
	GroupID  uint
	Template *Template
}

// TemplateReply is used in the RPC communications between the gateway and Communication
type TemplateReply struct {
	Template  *Template
	Templates []Template
}
